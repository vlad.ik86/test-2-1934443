
public class PayrollManagement 
{
	public static void main(String[] args)
	{
		Employee[] emp = new Employee[5];
		emp[0] = new SalariedEmployee(55000.0);
		emp[1] = new SalariedEmployee(65000.0);
		emp[2] = new HourlyEmployee(25.0, 10);
		emp[3] = new HourlyEmployee(30.0, 10);
		emp[4] = new UnionizedHourlyEmployee(45.0, 10.0, 30.0, 1.5);
		
		double totalWeeklyExpense = getTotalExpense(emp);
		
		System.out.println("Total weekly expense: $" + totalWeeklyExpense);
	}
	
	public static double getTotalExpense(Employee[] emp)
	{
		double total = 0;
		
		for(Employee e : emp)
		{
			total += e.getWeeklyPay();
		}
		
		return total;
	}
}
