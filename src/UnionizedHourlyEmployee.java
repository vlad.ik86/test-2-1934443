
public class UnionizedHourlyEmployee extends HourlyEmployee
{
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double hours, double pay, double maxHours, double overtime) 
	{
		super(hours, pay);
		
		this.maxHoursPerWeek = maxHours;
		this.overtimeRate = overtime;
	}
	
	@Override
	public double getWeeklyPay()
	{
		if(this.getWeekHours() <= this.maxHoursPerWeek)
		{
			return (this.getHourPay() * this.getWeekHours());
		}
		
		else
		{
			double beforeOvertime = this.maxHoursPerWeek * this.getHourPay();
			double afterOvertime = (this.getWeekHours() - this.maxHoursPerWeek) * this.getHourPay() * this.overtimeRate;
			
			return (beforeOvertime + afterOvertime);
		}
	}
}
