
public class HourlyEmployee implements Employee
{
	private double weekHours;
	private double hourPay;
	
	public HourlyEmployee(double hours, double pay)
	{
		this.weekHours = hours;
		this.hourPay = pay;
	}
	
	@Override
	public double getWeeklyPay() 
	{
		return (this.weekHours * this.hourPay);
	}
	
	public double getWeekHours()
	{
		return this.weekHours;
	}
	
	public double getHourPay()
	{
		return this.hourPay;
	}
}
