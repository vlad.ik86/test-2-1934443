
public class SalariedEmployee implements Employee
{
	private double yearlySalary;
	
	public SalariedEmployee(double sal)
	{
		this.yearlySalary = sal;
	}
	
	@Override
	public double getWeeklyPay() 
	{
		return yearlySalary /= 52;
	}
	
	public double getYearlySalary()
	{
		return this.yearlySalary;
	}
	
}
