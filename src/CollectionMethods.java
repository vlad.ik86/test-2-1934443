import java.util.*;

public class CollectionMethods 
{
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size)
	{
		Collection<Planet> newList = new ArrayList<Planet>();
		
		for(Planet p : planets)
		{
			if(p.getRadius() >= size)
			{
				newList.add(p);
			}
		}
		
		return newList;
	}
}
